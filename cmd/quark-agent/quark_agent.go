package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/quark_lt/pkg/util/agents/ssh_agent"
	"github.com/quark_lt/pkg/util/config"
	"log"
	"net/http"
	"os"
)

func main() {
	//todo исправить парсинг )))
	args := os.Args[1:]
	data, err := config.ParseSshConfig(args[1])
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(data.AuthMethod.UserAuth.Password)
	sshAgent := ssh_agent.NewSshAgent(&data)

	r := mux.NewRouter()
	r.HandleFunc("/ssh", func(w http.ResponseWriter, r *http.Request) {
		r.Header.Set("Content-Type", "application/json")

		metric := sshAgent.ReadMetric()
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(metric)
	})
	r.HandleFunc("/stop", func(w http.ResponseWriter, r *http.Request) {
		os.Exit(9)
	})
	log.Print(args[0])
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", args[0]), r))
}
